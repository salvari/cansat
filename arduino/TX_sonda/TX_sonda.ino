 
/*
 * TX_Sonda
 * El programa que se ejecuta en la sonda cansat
 */
 
#include <SPI.h>
#include <RH_RF69.h>
#include <RHReliableDatagram.h>


#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

// Definimos parámetros globales
#define RF69_FREQ 434.0                 // frecuencia de trabajo
#define RF69_TX_PWR 20                  // Potencia máxima de transmisión
//#define SEALEVELPRESSURE_HPA (1013.25)  // Presión atmosférica a nivel cero
#define SEALEVELPRESSURE_HPA (1025.00)
#define DELAY_TIME 383                 // Espera entre transmisiones (ajustada para conseguir 1 medida por segundo

// Definimos Id PARA COMUNICACIÓN RADIO
// Identificacion de estacion terrestre
#define DEST_ADDRESS   1
// Identificación de esta sonda
#define MY_ADDRESS     2

// Altura a la que se arma la secuencia de aterrizaje
#define ALTURA_ARMADO 100

// Altura a la que se inicia la secuencia de aterrizaje (alarma encendida!!)
#define ALTURA_LIMITE 50

// Definimos pines de la radio NO TOCAR 
#define RFM69_CS      8
#define RFM69_INT     3
#define RFM69_RST     4
#define LED           13


// Definimos pines del LED y el Zumbador 
#define LED_TIERRA    11
#define ZUMBA         12


// definimos intervalos morse
#define RAYA   400
#define PUNTO  150
#define SEPARA 100




float altura = 0;      // para tener la altrua en una variable global
bool ARMADO = false;   // estado de la secuencia de aterrizaje false = desarmado

// Creamos un objeto gestor de la radio
RH_RF69 rf69(RFM69_CS, RFM69_INT);

// y una clase para envio de mensajes usando el gestor de radio
RHReliableDatagram rf69_manager(rf69, MY_ADDRESS);

int16_t packetnum = 0;  // contador de paquetes

// Creamos un objeto gestor del sensor
Adafruit_BME280 bme; // I2C

void setup() {
  Serial.begin(115200);   // preparamos el puerto serie 

  pinMode(LED, OUTPUT);          // el pin que controla el led de usuario como salida   
  pinMode(LED_TIERRA, OUTPUT);   // el pin que controla el led de aterrizaje como salida
  pinMode(ZUMBA, OUTPUT);        // el pin que controla el zumbador de aterrizaje como salida
  pinMode(RFM69_RST, OUTPUT);    // el pin que resetea la radio como salida
  digitalWrite(RFM69_RST, LOW);  // y en estado "desactivado"

  Serial.println("Vamos a probar la radio:");
  Serial.println("Prueba de Tx");
  Serial.println();

  // hacemos un reset de la radio
  digitalWrite(RFM69_RST, HIGH);
  delay(10);
  digitalWrite(RFM69_RST, LOW);
  delay(10);

  if (!rf69_manager.init()) {                      // si la radio no arranca
    Serial.println("RFM69: La radio no arranca");  // damos mensaje de error
    while (1);                                     // nos quedamos aqui para siempre (nunca ha pasado)
  }


  Serial.println("RFM69 radio encendida: OK");
  // establecemos la frecuencia de trabajo
  if (!rf69.setFrequency(RF69_FREQ)) {
    Serial.println("ERROR: No se puede establecer la frecuencia de trabajo");
  }

  // Configura la potencia de transmisión
  rf69.setTxPower(RF69_TX_PWR, true);  // range from 14-20 for power, 2nd arg must be true for 69HCW

  // Configura la clave de cifrado
  // The encryption key has to be the same as the one in the server
  uint8_t key[] = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
  rf69.setEncryptionKey(key);

  pinMode(LED, OUTPUT);

  Serial.print("RFM69 radio Freq: ");  Serial.print((int)RF69_FREQ);   Serial.println(" MHz");
  Serial.print("RFM69 radio TxPw: ");  Serial.print((int)RF69_TX_PWR); Serial.println(" dBm");
  Serial.print("RFM69 MSG MX LEN: ");  Serial.println((int)RH_RF69_MAX_MESSAGE_LEN);
  Serial.println();

  Serial.println("Probando sensor BME280");
  unsigned status;
    
  // leemos parametros por defecto
  status = bme.begin();
  if (!status) {
        Serial.println("Error en sensor BME280: no hay comunicación con el sensor");
        while (1);
  }
  Serial.println("Sensor BME280: OK");
  Serial.println();

}

// Dont put this on the stack:
uint8_t buf[RH_RF69_MAX_MESSAGE_LEN];
uint8_t data[] = "  OK";


void loop() {
  delay(DELAY_TIME);  // Un sg entre transmisiones

  // Preparamos el mensaje
  String msg = String(packetnum++) + getBMEvalues();
  // lo mandamos al puerto serie (solo para depurar)
  Serial.print("Sending "); Serial.println(msg);

  char radiopacket[60];
  msg.toCharArray(radiopacket, 60);
  Serial.println(radiopacket);
    // Enviamos el mensaje a la estación terrena
  if (rf69_manager.sendtoWait((uint8_t *)radiopacket, strlen(radiopacket), DEST_ADDRESS)) {
    // y esperamos respuesta de la estacion
    // según lo que pase sacamos distintos mensajes por el puerto serie
    uint8_t len = sizeof(buf);
    uint8_t from;   
    if (rf69_manager.recvfromAckTimeout(buf, &len, 2000, &from)) {
      buf[len] = 0; // zero out remaining string
      
      Serial.print("Respuesta recibida de #"); Serial.print(from);
      Serial.print(" [RSSI :");
      Serial.print(rf69.lastRssi());
      Serial.print("] : ");
      Serial.println((char*)buf);     
      Blink(LED, 40, 3); //blink LED 3 times, 40ms between blinks
    } else {
      Serial.println("SIN RESPUESTA :(");
    }
  } else {
    Serial.println("No tenemos ACK");
  }
  // Si la secuencia de aterrizaje esta ARMADA y bajamos de la altura limite enviamos SOS
  if(ARMADO && (altura < ALTURA_LIMITE)) {
    senalTierra();
  }
}


/*
 * getBMEvalues
 * Lee los valores de la sonda de presión temperatura y humedad
 * Devuelve una cadena con los valores separados por /
 */
String getBMEvalues() {
  unsigned status = bme.begin();
  altura = bme.readAltitude(SEALEVELPRESSURE_HPA);
  if (altura >= ALTURA_ARMADO) {
    ARMADO = true;
  }
  String myValue = "/";
  myValue += bme.readAltitude(SEALEVELPRESSURE_HPA);
  myValue += "/";
  myValue += bme.readTemperature();
  myValue += "/";
  myValue += bme.readHumidity();
  myValue += "/";
  myValue += (bme.readPressure() / 100.00F);
  return myValue;
}

/*
 * Blink, hace parpadear el led de usuario
 * Para indicar que la radio está funcionando
 */
void Blink(byte PIN, byte DELAY_MS, byte loops) {
  for (byte i=0; i<loops; i++)  {
    digitalWrite(PIN,HIGH);
    delay(DELAY_MS);
    digitalWrite(PIN,LOW);
    delay(DELAY_MS);
  }
}


/*
 * senalTierra
 * Ejecuta la señal SOS para poder localizar la sonda tras el aterrizaje
 */
void senalTierra() {
  S();
  delay(SEPARA);
   O();
  delay(SEPARA);
    S();
  delay(SEPARA);
  
}


/*
 * S()
 * Codifica una letra S en morse, con luz y sonido
 */
void S() {
  Serial.println("Señal!");
 for(int x=0; x<3; x++){
   digitalWrite(LED_TIERRA, HIGH);
   digitalWrite(ZUMBA, HIGH);  
   delay(PUNTO);
   digitalWrite(LED_TIERRA, LOW);
   digitalWrite(ZUMBA, LOW);
   delay(SEPARA);

 }
}

/*
 * O()
 * Codifica una letra O en morse, con luz y sonido
 */
void O() {
  Serial.println("Señal!");
 for(int x=0; x<3; x++){
   digitalWrite(LED_TIERRA, HIGH);
   digitalWrite(ZUMBA, HIGH);
   delay(RAYA);
   digitalWrite(LED_TIERRA, LOW);
   digitalWrite(ZUMBA, LOW);
   delay(SEPARA);

 }
}
