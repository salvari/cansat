import processing.serial.*;  // Importamos todos los metodos de serial

/// ----- constantes -----------------------------------------------------------
int TIEMPO_LIMITE = 2000;   // milisegundos que tienen que pasar sin recepción para dar el enlace por malo

/// ----- variables ------------------------------------------------------------
Serial miPuerto;
int   mensaje;
float alt;
float temp;
float hum;
float pres;
float rssi;

int millisOrigen;                   // milisegundos al empezar
int millisUltimoMensaje;            // el momento de recepcepción del ultimo paquete
int periodo;                        // milisg entre los dos últimos mensajes
int segundosMision;                  // segundos funcionando

int resetsEnlace = 0;               // Resets de enlace acumulados.

FloatList rssiList = new FloatList();
FloatList humList  = new FloatList();
FloatList altList  = new FloatList();
FloatList presList = new FloatList();
FloatList tempList = new FloatList();
IntList milList    = new IntList();
IntList mensList   = new IntList();

PrintWriter output;

/*----------------------------------------------------------------------
  setup
  Esta función establece los valores iniciales para configurar processing
  ----------------------------------------------------------------------*/
void setup() {
  // size(1000, 500);                 // Preparamos el area de trabajo
  fullScreen();
  millisOrigen = millis();
  if (Serial.list().length > 0) {
    print(Serial.list()[0]);
    miPuerto = new Serial(this, Serial.list()[0], 9600);
    miPuerto.bufferUntil('\n');
    output = createWriter("datalog" + hour() + minute() + second() + ".txt");
  }

}

void draw() {
  mostrarValoresInstantaneos();
  mostrarEnlace();
  graficaComun();
}



/*----------------------------------------------------------------------
  serialEvent
  Esta funcion se llama cada vez que hay datos en el puerto serie
  ----------------------------------------------------------------------*/
void serialEvent(Serial puerto) {
  String contenidoSerie;
  int ahora = millis();
  periodo = ahora - millisUltimoMensaje;
  millisUltimoMensaje = ahora;
  contenidoSerie = puerto.readString();             // leemos el contenido del puerto serie
  if (contenidoSerie != null) {                     // si tiene datos
    print(contenidoSerie);
    output.print(contenidoSerie);
    procesaDatos(contenidoSerie);
  }
}

/*----------------------------------------------------------------------
  resetSerial
  Esta funcion resetea el puerto serie
  Se invoca si falla el enlace
  ----------------------------------------------------------------------*/
void resetSerial() {
  miPuerto.stop();
  delay(250);
  miPuerto = new Serial(this, Serial.list()[0], 9600);
  miPuerto.bufferUntil('\n');
  text("RESET DE ENLACE", 500, 30);
  resetsEnlace++;
  millisUltimoMensaje = millis();                       // Para no entrar en bucle de resets
}

/*----------------------------------------------------------------------
  procesaDatos
  Esta funcion actualiza
  - Los valores de los datos instantaneos
  - Las listas de valores recibidos
  ----------------------------------------------------------------------*/
void procesaDatos (String linea) {
  String[] valores = split(trim(linea), '/');
  if (valores.length == 6){
    milList.append(millisUltimoMensaje);                     
    mensaje = int(valores[0]);
    alt  = float(valores[1]);
    temp = float(valores[2]);
    hum  = float(valores[3]);
    pres = float(valores[4]);
    rssi = float(valores[5]);
    mensList.append(mensaje);
    altList.append(alt);
    tempList.append(temp);
    humList.append(hum);
    presList.append(pres);
    rssiList.append(rssi);
    println("Mensajes guardados: " + mensList.size());
  } else {
    println("Linea incompleta");
  }
  println("Mensajes guardados: " + mensList.size());
}

/*----------------------------------------------------------------------
  mostrarValoresInstantaneos
  En cada ciclo 'draw' refresca los valores instantaneos recibidos de la
  sonda en la pantalla (esquina superior izquierda)
  ----------------------------------------------------------------------*/
void mostrarValoresInstantaneos(){
  fill(150);
  rect(5, 5, 300, 130);
  fill(255);
  textSize(32);
  text("Altura " + alt + " m.", 10, 35);
  text("Temp. " + temp + " C", 10, 65);
  text("Hum. " + hum + "%", 10, 95);
  text("Pres." + pres + "hPa", 10, 125);
}

/*----------------------------------------------------------------------
  mostrarEnlace
  En cada ciclo 'draw' refresca los datos del enlace, si el estado del
  enlace es malo resetea el puerto serie (hemos comprobado que los
  problemas de comunicación se dan en el puerto serie y no en el enlace
  radio.
  ----------------------------------------------------------------------*/
void mostrarEnlace(){
    fill(150);
    rect(310, 5, width - 315, 130);
    fill(255);
    textSize(32);
    if ((millis() - millisUltimoMensaje) < TIEMPO_LIMITE) {
      fill(#00FF00);
      text("Enlace OK", 315, 35);
      fill(255);
    } else {
      fill(#0000FF);
      text("Enlace KO", 315, 35);
      fill(255);
      if ( (millis() - millisOrigen) > 2000) {
        resetSerial();
      }
      
    }
    text("rssi " + rssi, 315, 65);
    text("Per. " + periodo, 315, 95);
    text("Resets: " + resetsEnlace, 315, 125);
}


void graficaComun() {
  fill(150);
  rect(5, 140, (width - 10), (height - 145));
  fill(255);
  rect(150, 145, (width - 160), (height - 155));
  
  // lineas de escala horizontales
  strokeWeight(1);
  stroke(0);
  int medio = floor((height-155)/2) +145;
  int cuarto = floor((height-155)/4);
  line (145, medio, width-10, medio);
  line (145, medio-cuarto, width-10, medio-cuarto);
  line (145, medio+cuarto, width-10, medio+cuarto);
  
  //leyenda
  textSize(18);
  fill(#0000FF);
  text("Humedad", 5, height - 120);
  text("0 - 100 %", 5, height - 100);
  fill(#FFC903);
  text("Temp.", 5, height - 160);
  text("-30 a 30 Cels.", 5, height - 140);
  fill(#00FF00);
  text("Pres.", 5, height - 200);
  text("800 a 1050 hPa", 5, height - 180);
  fill(#FF0000);
  text("Altura.", 5, height - 240);
  text("0 a 1200 m.", 5, height - 220);
  
  fill(150);
  if (milList.size() >= 2) { 
    int ultimo = milList.size() - 1;
    stroke(0);
    strokeWeight(3);
    for (int i=0; i < ultimo -1; i++){
      int x1 = coordX(width-160, milList.get(i));
      int x2 = coordX(width-160, milList.get(i+1));
      stroke(#0000FF);  // azul para humedad relativa (de 0 a 100 por ciento)
      line(x1, floor((100 - humList.get(i)) * (height - 155) / 100) + 145, x2, floor((100 - humList.get(i+1)) * (height - 155) / 100) + 145);
      stroke(#FFC903);  // naranja para temperatura (de -30 a 30 grados)
      line(x1, floor((30 - tempList.get(i)) * (height - 155) / 60) + 145, x2, floor((30 - tempList.get(i+1)) * (height - 155) / 60) + 145);
      stroke(#00FF00);  // verde para presión atmosférica (desde 800 a 1050 hPa)
      line(x1, floor((1050 - presList.get(i)) * (height - 155) / 250) + 145, x2, floor((1050 - presList.get(i+1)) * (height - 155) / 250) + 145);
      stroke(#FF0000);  // rojo para altura (de 0 a 1200 mts)
      line(x1, floor((1200 - altList.get(i)) * (height - 155) / 1200) + 145, x2, floor((1200 - altList.get(i+1)) * (height - 155) / 1200) + 145);
    }
    strokeWeight(1);
    stroke(0);
    // ahora pintamos las referencias de tiempo
    segundosMision = floor((milList.get(ultimo) - milList.get(0))/1000);
    int escala = 1;
    // print(coordX(width-160, 0) + "-" + segundosMision + "///");
    if (segundosMision <= 60) {
      escala = 1;
    } else if (segundosMision <= 120){
      escala = 2;
    } else if (segundosMision <= 300){
      escala = 5;
    } else if (segundosMision <= 900) {
      escala = 10;
    } else escala = 30;
    textSize(18);
    fill(255);
    text("esc.T. " + escala + " sg.", 10, height - 20);
    
    for (int i=0; i < segundosMision; i = i+escala){
      int x = coordX(width-160, i*1000 + milList.get(0)); 
      line(x, 145, x, height -10); 
    }
  }
}

int coordX (int ancho, int milsg) {
  return floor((milsg - milList.get(0)) * ancho / (milList.get(milList.size()-1) - milList.get(0))) + 150;
}

void keyPressed() {
  if (key == 'q') {
    output.flush(); // Writes the remaining data to the file
    output.close(); // Finishes the file
    exit(); // Stops the program
  }
}
